const express = require('express');
const cors = require('cors');

const React = require('./app/routers/React');
const Chat = require('./app/routers/Chat');
const ChatSocket = require('./app/controllers/ChatSocket');

global.config = require('./config.json');

// * Setup express
const app = express();
app.disable("x-powered-by");//Disable express fingerprint
app.use(cors());// ! Enable CORS: DEBUG ONLY !

// * Setup socket.io
const server = require('http').createServer(app);
const socketIo = require('socket.io');
const StompitHandler = require('./app/controllers/StompitHandler');
const io = socketIo(server, {
    cors: {
        origin: "http://localhost:3000",
        methods: ["GET", "POST"]
    }
});


// * Setup stompit
const stompitHandler = new StompitHandler({to: global.config.topic});
stompitHandler.connect();

// * Setup chat socket
new ChatSocket(io, stompitHandler);

// * React app + Static react
//app.use(express.static(path.join(__dirname, '..', global.config.reactAppName, 'build')));

app.use('/chat', Chat);
app.use('/', React);
app.use(express.static(config.static));



// * Application start
server.listen(config.port, function () {
    console.log(`Express listening at ${config.port}`);
});