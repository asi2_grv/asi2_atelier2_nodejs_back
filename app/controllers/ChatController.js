const ChatService = require('../services/ChatService');

class ChatController {
    getUsers(req, res) {
        res.json(ChatService.getUsers());
    }
}

module.exports = new ChatController();