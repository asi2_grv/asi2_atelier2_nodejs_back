const Message = require('../model/Message');

const User = require('../model/User');
const ChatService = require('../services/ChatService');

class ChatSocket {
    constructor(io, stompitHandler) {
        this.handleConnection = this.handleConnection.bind(this);
        this.handleSetSenderId = this.handleSetSenderId.bind(this);
        this.handleSendMessage = this.handleSendMessage.bind(this);

        this.io = io;
        this.stompitHandler = stompitHandler;
        io.sockets.on('connection', this.handleConnection);
    }
    handleConnection(socket) {
        socket.on("disconnect", () => { this.handleDisconnect(socket) });
        socket.on('setSenderId', (data) => { this.handleSetSenderId(socket, data) });
        socket.on('sendMessage', (data) => { this.handleSendMessage(socket, data) });
    }
    handleSetSenderId(socket, { userId }) {
        if (userId === undefined) return;

        socket.join(userId);
        ChatService.addUser(new User({ socketId: socket.id, id: userId }));
    }
    handleSendMessage(socket, data) {
        const senderId = data.sender; // TODO retrieve by socket Id (socket creation with auth token ...)
        const targetId = data.receiver;
        if (senderId === undefined || targetId === undefined) return;

        const msg = new Message(data);
        socket.to(senderId).to(targetId).emit("getMessage", msg);

        this.stompitHandler.sendMessage("addMessage", msg)
    }
    handleDisconnect(socket) {
        //TODO: exit room ? + check who's in the room, if no one delete
        ChatService.removeUser(socket.id);
    }
}

module.exports = ChatSocket;