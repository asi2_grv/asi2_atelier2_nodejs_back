class ReactController {
    getApp(req, res) {
        res.sendFile(path.join(__dirname, '..', global.config.reactAppName, 'build', 'index.html'));
    }
}

module.exports = new ReactController();