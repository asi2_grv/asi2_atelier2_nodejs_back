class Message {
    constructor({ sender, receiver, content }) {
        this.sender = Number(sender);
        this.receiver = Number(receiver);
        this.content = content ? String(content) : "";
        this.time = Math.floor(+new Date() / 1000);
    }
}
module.exports = Message;