class User {
    constructor({socketId, id}) {
        this.socketId = socketId;
        this.id = Number(id);
    }
}
module.exports = User;