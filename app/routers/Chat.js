const { Router } = require('express');
const ChatController = require('../controllers/ChatController');

const BASE_PATH = '';

const ChatRouter = Router();
module.exports = ChatRouter;

ChatRouter.route(`${BASE_PATH}/users`)
    .get(ChatController.getUsers);
