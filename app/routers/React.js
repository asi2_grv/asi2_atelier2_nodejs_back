const {Router} = require('express');
const ReactController = require('../controllers/ReactController');

const BASE_PATH = '/';

const ReactRouter = Router();
module.exports = ReactRouter;

ReactRouter.route(BASE_PATH)
    .get(ReactController.getApp);
