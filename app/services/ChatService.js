class ChatService {
    constructor() {
        this.users = {};
    }
    addUser(user) {
        this.users[user.socketId] = user;
    }
    removeUser(socketId) {
        delete this.users[socketId];
    }
    getUsers() {
        return [...new Set(Object.values(this.users).map(user => user.id))];
    }
}
module.exports = new ChatService();